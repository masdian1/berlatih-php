<?php
function ubah_huruf($string){
    $a = "";
    $b = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $a = ord($string[$i]) + 1;
        if ($a > 122) {
            $a = 97;
        }
        $b .= chr($a);
    }
    echo $b; echo "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>